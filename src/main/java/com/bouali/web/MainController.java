package com.bouali.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/home")
public class MainController {

	@RequestMapping(value = "/")
	public String index(Model model) {
		return "home/home";
	}
}
